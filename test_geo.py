from floodsystem.station import MonitoringStation
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river
from floodsystem.geo import stations_by_distance
from floodsystem.geo import stations_within_radius
from floodsystem.geo import rivers_by_station_number

def test_rivers_with_station():
   # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations = [s]
    assert rivers_with_station(stations) == {s.river}

def test_stations_by_river():
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations = [s]
    assert stations_by_river(stations) == {s.river: [s]}

def test_stations_by_distance():
    # Create a station
    s_id = "test-s-id-1"
    m_id = "test-m-id-1"
    label = "1st station"
    coord = (-2.0, 2.0)
    trange = (0.0, 4.0)
    river = "River X"
    town = "Town 1"
    station_1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create another station
    s_id = "test-s-id-2"
    m_id = "test-m-id-2"
    label = "2nd station"
    coord = (-1.0, 1.0)
    trange = (0.0, 2.0)
    river = "River Y"
    town = "Town 2"
    station_2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations = [station_1, station_2]

    assert stations_by_distance(stations, (0.0, 0.0))[0][0] == station_2
    assert stations_by_distance(stations, (0.0, 0.0))[1][0] == station_1
    assert round(stations_by_distance(stations, (0.0, 0.0))[0][1], 3) == 157.250
    assert round(stations_by_distance(stations, (0.0, 0.0))[1][1], 3) == 314.475

def test_stations_within_radius():
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (1.0, 1.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations = [s]
 
    assert stations_within_radius(stations, (0.0, 0.0), 200.0) == [s]

def test_rivers_by_station_number():
    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (1.0, 1.0)
    trange = (0.0, 1.0)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    stations =[s]

    assert rivers_by_station_number(stations, 1) == [(s.river, 1)]