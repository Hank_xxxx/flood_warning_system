from floodsystem.geo import stations_by_distance

from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()
    
    # Sort stations by distance
    stations = stations_by_distance(stations, (52.2053, 0.1218))
    
    # Print the closest and furthest 10 stations
    closest_10_stations = []
    furthest_10_stations = []
    for station in stations[:10]:
        closest_10_stations.append((station[0].name, station[0].town, station[1]))
    for station in stations[-10:]:
        furthest_10_stations.append((station[0].name, station[0].town, station[1]))
    print (closest_10_stations)
    print (furthest_10_stations)


if __name__ == "__main__":
    print("*** Task 1B: CUED Part IA Flood Warning System ***")
    run()