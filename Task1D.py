from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station
from floodsystem.geo import stations_by_river

def run():
    """Requirements for Task 1D"""

    # Build list of stations
    stations = build_station_list()

    # Find all the rivers with a monitoring station
    rivers = list(rivers_with_station(stations))

    # Print the number of rivers
    print (len(rivers))

    # Print the first 10 rivers in alphabetical order
    rivers.sort()
    print (rivers[:10])

    station_by_river = stations_by_river(stations)

    # Print stations on the three rivers
    river_aire_station = station_by_river["River Aire"]
    river_aire_station_name = []
    for station in river_aire_station:
        river_aire_station_name.append(station.name)
    river_aire_station_name.sort()
    print ("Stations on River Aire: {}".format(river_aire_station_name))
    river_cam_station = station_by_river["River Cam"]
    river_cam_station_name = []
    for station in river_cam_station:
        river_cam_station_name.append(station.name)
    river_cam_station_name.sort()
    print ("Stations on River Cam: {}".format(river_cam_station_name))
    river_thames_station = station_by_river["River Thames"]
    river_thames_station_name = []
    for station in river_thames_station:
        river_thames_station_name.append(station.name)
    river_thames_station_name.sort()
    print ("Stations on River Thames: {}".format(river_thames_station_name))


if __name__ == "__main__":
    print("*** Task 1D: CUED Part IA Flood Warning System ***")
    run()