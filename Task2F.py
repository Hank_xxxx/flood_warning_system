import datetime
from floodsystem.plot import plot_water_level_with_fit
from floodsystem.stationdata import update_water_levels
from floodsystem.flood import stations_highest_rel_level
from floodsystem.stationdata import build_station_list
from floodsystem.datafetcher import fetch_measure_levels

def run():
    # Build list of stations
    stations = build_station_list()

    update_water_levels(stations)

    selected_stations = stations_highest_rel_level(stations, 5)

    for station in selected_stations:
        dt = 2
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
        if dates != [] and levels != []:
            plot_water_level_with_fit(station, dates, levels, 4)

if __name__ == "__main__":
    print("*** Task 2F: CUED Part IA Flood Warning System ***")
    run()