from floodsystem.flood import stations_highest_rel_level, stations_level_over_threshold
from floodsystem.station import MonitoringStation

def test_stations_level_over_threshold():
    # Create a station
    s_id = "test-s-id-1"
    m_id = "test-m-id-1"
    label = "1st station"
    coord = (-2.0, 4.0)
    trange = (0.0, 4.0)
    river = "River X"
    town = "Town 1"
    station_1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create another station
    s_id = "test-s-id-2"
    m_id = "test-m-id-2"
    label = "2nd station"
    coord = (-4.0, 2.0)
    trange = (0.0, 2.0)
    river = "River Y"
    town = "Town 2"
    station_2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    station_1.latest_level = 8.0

    station_2.latest_level = 1.0

    stations = [station_1, station_2] 

    assert stations_level_over_threshold(stations, 1.0) == [(station_1, 2.0)]

def test_stations_highest_rel_level():
    # Create a station
    s_id = "test-s-id-1"
    m_id = "test-m-id-1"
    label = "1st station"
    coord = (-2.0, 4.0)
    trange = (0.0, 4.0)
    river = "River X"
    town = "Town 1"
    station_1 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    # Create another station
    s_id = "test-s-id-2"
    m_id = "test-m-id-2"
    label = "2nd station"
    coord = (-4.0, 2.0)
    trange = (0.0, 2.0)
    river = "River Y"
    town = "Town 2"
    station_2 = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    station_1.latest_level = 8.0

    station_2.latest_level = 1.0

    stations = [station_1, station_2]

    assert stations_highest_rel_level(stations, 1) == [station_1]