from floodsystem.analysis import polyfit
from matplotlib.dates import date2num
import datetime

def test_polyfit():
    dates = [datetime.datetime(2000, 1, 1, 0), datetime.datetime(2000, 1, 1, 4), datetime.datetime(2000, 1, 1, 8), datetime.datetime(2000, 1, 1, 12), datetime.datetime(2000, 1, 1, 16), datetime.datetime(2000, 1, 1, 20)]
    levels = [0.0, 4.0, 8.0, 12.0, 16.0, 20.0]
    poly, d0 = polyfit(dates, levels, 1)

    assert round(poly(d0[0]), 3) == 0.000
    assert round(poly(d0[1]), 3) == 4.000
    assert round(poly(d0[2]), 3) == 8.000
    assert round(poly(d0[3]), 3) == 12.000
    assert round(poly(d0[4]), 3) == 16.000
    assert round(poly(d0[5]), 3) == 20.000