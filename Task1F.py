from floodsystem.stationdata import build_station_list

from floodsystem.station import inconsistent_typical_range_stations

def run():
    """Requirements for Task 1F"""

    # Build list of stations
    stations = build_station_list()
    
    # Find all the stations with inconsistent typical range
    stations = inconsistent_typical_range_stations(stations)

    # Print all the station names
    station_names = []
    for station in stations:
        station_names.append(station.name)
    print (station_names)


if __name__ == "__main__":
    print("*** Task 1F: CUED Part IA Flood Warning System ***")
    run()