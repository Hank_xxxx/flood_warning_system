import numpy as np
import matplotlib.pyplot as plt
from matplotlib.dates import date2num
import datetime
from .datafetcher import fetch_measure_levels

def polyfit(dates, levels, p):
    # Find coefficients of best-fit polynomial f(x) of degree p
    num_date = date2num(dates)
    p_coeff = np.polyfit(num_date - num_date[0], levels, p)

    # Convert coefficient into a polynomial that can be evaluated,
    # e.g. poly(0.3)
    poly = np.poly1d(p_coeff)

    d0 = num_date - num_date[0]

    return poly, d0

def gradient(station):
    dt = 1
    try:
        dates, levels = fetch_measure_levels(station.measure_id, dt=datetime.timedelta(days=dt))
    except:
        return None

    # Find coefficients of best-fit polynomial f(x) of degree 1
    num_date = date2num(dates)
    try:
        p_coeff = np.polyfit(num_date - num_date[0], levels, 1)
        return p_coeff[0]
    except:
        return None