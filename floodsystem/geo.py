# Copyright (C) 2018 Garth N. Wells
#
# SPDX-License-Identifier: MIT
"""This module contains a collection of functions related to
geographical data.

"""

from .utils import sorted_by_key  # noqa

from haversine import haversine

def stations_by_distance(stations, p):
    """
    Sorting stations by distance
    Return tuples of stations and distances
    """
    stations_and_distances = []
    for station in stations:
        distance = haversine(station.coord, p)
        stations_and_distances.append((station, distance))
    
    # Sort stations by distance
    stations_and_distances = sorted_by_key(stations_and_distances, 1)
    return stations_and_distances

def stations_within_radius(stations, centre, r):
    """
    Get all the stations within a certain radius around a centre
    """
    stations_within_radius = []
    for station in stations:
        distance = haversine(station.coord, centre)
        if distance < r:
            stations_within_radius.append(station)
    return stations_within_radius

def rivers_by_station_number(stations, N):
    """
    For a list of stations
    get the number of stations on each river
    then sort the rivers by the number of stations
    """

    # Find the number of stations on each river

    dict_rivers_and_station_number = {}
    for station in stations:
        if station.river in dict_rivers_and_station_number:
            dict_rivers_and_station_number[station.river] += 1
        else:
            dict_rivers_and_station_number[station.river] = 1
    list_rivers_and_station_number = []
    for river_name, station_number in dict_rivers_and_station_number.items():
        list_rivers_and_station_number.append((river_name, station_number))
    
    # Sort the rivers by station number

    rivers_and_station_number_sorted = sorted_by_key(list_rivers_and_station_number, 1, reverse=True)

    # Find the N rivers with the greatest number of monitoring stations
    
    result = []
    for item in rivers_and_station_number_sorted:
        if item[1] >= rivers_and_station_number_sorted[N-1][1]:
            result.append(item)
        else:
            break
    return result

def rivers_with_station(stations):
    """
    For a list of stations
    get all the rivers with a monitoring station
    """

    rivers = set()

    for station in stations:
        rivers.add(station.river)
    
    return rivers

def stations_by_river(stations):
    """
    For a list of stations
    map rivers to a list of stations on the river
    """

    stations_on_river = {}

    for station in stations:
        if station.river in stations_on_river:
            stations_on_river[station.river].append(station)
        else:
            stations_on_river[station.river] = [station]
    
    return stations_on_river