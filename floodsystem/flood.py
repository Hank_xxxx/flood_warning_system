from .station import MonitoringStation

from .utils import sorted_by_key

def stations_level_over_threshold(stations, tol):
    result = []
    for station in stations:
        if type(MonitoringStation.relative_water_level(station)) == float:
                if MonitoringStation.relative_water_level(station) > tol:
                        result.append((station, MonitoringStation.relative_water_level(station)))
    result = sorted_by_key(result, 1, True)
    return result

def stations_level_over_typical_range(stations):
    result = []
    for station in stations:
        relative_water_level = station.relative_water_level()
        if type(relative_water_level) == float:
                if relative_water_level > 1.0:
                        result.append((station, relative_water_level))
    return result

def stations_level_within_typical_range(stations):
    result = []
    for station in stations:
        relative_water_level = station.relative_water_level()
        if type(relative_water_level) == float:
                if relative_water_level >= 0.0 and relative_water_level <= 1.0:
                        result.append((station, relative_water_level))
    return result

def stations_level_below_typical_low(stations):
    result = []
    for station in stations:
        relative_water_level = station.relative_water_level()
        if type(relative_water_level) == float:
                if relative_water_level < 0.0:
                        result.append((station, relative_water_level))
    return result

def stations_highest_rel_level(stations, N):
        stations_and_relative_level = []
        for station in stations:
                if type(MonitoringStation.relative_water_level(station)) == float:
                        stations_and_relative_level.append((station, station.relative_water_level()))
        stations_and_relative_level_sorted = sorted_by_key(stations_and_relative_level, 1, True)
        result = []
        if stations_and_relative_level_sorted != []:
                for i in range(N):
                        result.append(stations_and_relative_level_sorted[i][0])
        return result