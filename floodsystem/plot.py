import matplotlib.pyplot as plt
from datetime import datetime, timedelta
from .analysis import polyfit

def plot_water_levels(station, dates, levels):
    # Plot
    plt.plot(dates, levels)
    plt.hlines(station.typical_range[0], dates[0], dates[-1])
    plt.hlines(station.typical_range[1], dates[0], dates[-1])
    
    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    plt.show()

def plot_water_level_with_fit(station, dates, levels, p):
    # Plot original data points
    plt.plot(dates, levels, '.')
    plt.hlines(station.typical_range[0], dates[0], dates[-1])
    plt.hlines(station.typical_range[1], dates[0], dates[-1])

    # Plot polynomial fit at 30 points along interval
    poly, d0 = polyfit(dates, levels, p)
    plt.plot(dates, poly(d0))

    # Add axis labels, rotate date labels and add plot title
    plt.xlabel('date')
    plt.ylabel('water level (m)')
    plt.xticks(rotation=45)
    plt.title(station.name)

    # Display plot
    plt.tight_layout()  # This makes sure plot does not cut off date labels

    # Display plot
    plt.show()   