from floodsystem.stationdata import build_station_list, update_water_levels
from floodsystem.flood import stations_level_over_typical_range, stations_level_within_typical_range, stations_level_below_typical_low
from floodsystem.analysis import gradient
from floodsystem.utils import sorted_by_key

def run():
    # Build list of stations
    stations = build_station_list()

    update_water_levels(stations)

    # Divide stations into different risk levels
    stations_over_typical_high = stations_level_over_typical_range(stations)

    stations_within_typical_range = stations_level_within_typical_range(stations)

    stations_below_typical_low = stations_level_below_typical_low(stations)

    low_risk_stations = []
    moderate_risk_stations = []
    high_risk_stations = []
    severe_risk_stations = []

    for station in stations_below_typical_low:
        k = gradient(station[0])
        if isinstance(k, float):
            low_risk_stations.append((station[0], station[1], k))

    for station in stations_within_typical_range:
        k = gradient(station[0])
        if isinstance(k, float):
            if k <= 0.0:
                low_risk_stations.append((station[0], station[1], k))
            else:
                moderate_risk_stations.append((station[0], station[1], k, station[1]*k))
    
    for station in stations_over_typical_high:
        k = gradient(station[0])
        if isinstance(k, float):
            if k <= 0:
                high_risk_stations.append((station[0], station[1], k, -station[1]/k))
            else:
                severe_risk_stations.append((station[0], station[1], k, station[1]*k))
    
    # Sort stations into order
    low_risk_stations = sorted_by_key(low_risk_stations, 1, True)
    moderate_risk_stations = sorted_by_key(moderate_risk_stations, 3, True)
    high_risk_stations = sorted_by_key(high_risk_stations, 3, True)
    severe_risk_stations = sorted_by_key(severe_risk_stations, 3, True)

    # Print results
    print()
    print("*** Low Risk Stations ***")
    print()
    for station in low_risk_stations:
        print("Station: {}, Relative Water Level: {}, Water Level Change Rate: {}".format(station[0].name, station[1], station[2]))
    print()
    print("*** Moderate Risk Stations ***")
    print()
    for station in moderate_risk_stations:
        print("Station: {}, Relative Water Level: {}, Water Level Change Rate: {}, Risk Coefficent: {}".format(station[0].name, station[1], station[2], station[3]))
    print()
    print("*** High Risk Stations ***")
    print()
    for station in high_risk_stations:
        print("Station: {}, Relative Water Level: {}, Water Level Change Rate: {}, Risk Coefficent: {}".format(station[0].name, station[1], station[2], station[3]))
    print()
    print("*** Severe Risk Stations ***")
    print()
    for station in severe_risk_stations:
        print("Station: {}, Relative Water Level: {}, Water Level Change Rate: {}, Risk Coefficent: {}".format(station[0].name, station[1], station[2], station[3]))
    print()
    print("*** END ***")

if __name__ == "__main__":
    print("*** Task 2G: CUED Part IA Flood Warning System ***")
    run()