from floodsystem.geo import stations_within_radius

from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()
    
    # Find all the stations within the radius
    stations = stations_within_radius(stations, (52.2053, 0.1218), 10)
    
    # Print all the stations within the radius
    station_names = []
    for station in stations:
        station_names.append(station.name)
    station_names.sort()
    print (station_names)


if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    run()